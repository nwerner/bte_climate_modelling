# Running PlaSim-GENIE on ETH EULER Cluster
If you have questions or troubles you can add them to [**this formular**](https://docs.google.com/forms/d/1CNCCny9PWvZ_prP3G6GRPsAGZO_74INcrkO0XE0uehM/edit) and I'll get notified and try to answer them.

## What is PlaSim-GENIE and why would you want to use it?

PlaSim-GENIE is an Earth System Model (ESM) of intermediate complexity, meaning that it includes some simpler parameterizations of  some physics compared to a full fledged ESM like the ones used in the scope of the Climate Model Intercomparison Project (CMIP) in the scope of the IPCC Reports.

What’s the point of not having all the complex physics?
Complex ESMs take a long time to run simulations with, even on supercomputers. This might be fine for simulations of e.g. 200 years (which might take 2 weeks to run). However the computational cost gets out of hand if you want to simulate climate over Earth history. Since some physical processes (such as cloud microphysics) might not play a critical role in paleo-simulations (since you have no proxy data anyway to properly validate your model results to the 5th digit of temperature change or precipitation). Therefore intermediate complexity models (EMICs) can be a useful tool to investigate larger-scale mechanisms that govern Earth’s climate, and you are able to run 200 years in a couple of hours.

[**PlaSim reference manual**](https://www.seao2.info/cgenie/docs/muffin.pdf)
[**cGENIE reference manual**](https://www.mi.uni-hamburg.de/en/arbeitsgruppen/theoretische-meteorologie/modelle/sources/psreferencemanual-1.pdf)

Here are some papers that might contain some useful information for your work with PlaSim-GENIE and are a good starter to understand what the model is actually made for.

#### Papers with/about PlaSim-GENIE: 

[Barreto, E., Holden, P. B., Edwards, N. R., & Rangel, T. F. (2023). PALEO‐PGEM‐Series: A spatial time series of the global climate over the last 5 million years (Plio‐Pleistocene). Global Ecology and Biogeography.](https://onlinelibrary.wiley.com/doi/full/10.1111/geb.13683)

[Holden, P. B., Edwards, N. R., Rangel, T. F., Pereira, E. B., Tran, G. T., & Wilkinson, R. D. (2019). PALEO-PGEM v1. 0: a statistical emulator of Pliocene–Pleistocene climate. Geoscientific Model Development, 12(12), 5137-5155.](https://gmd.copernicus.org/articles/12/5137/2019/)

[Holden, P. B., Edwards, N. R., Fraedrich, K., Kirk, E., Lunkeit, F., & Zhu, X. (2016). PLASIM–GENIE v1. 0: a new intermediate complexity AOGCM. Geoscientific Model Development, 9(9), 3347-3361.](https://gmd.copernicus.org/articles/9/3347/2016/)

[Holden, P. B., Edwards, N. R., Ridgwell, A., Wilkinson, R. D., Fraedrich, K., Lunkeit, F., ... & Viñuales, J. E. (2018). Climate–carbon cycle uncertainties and the Paris Agreement. Nature Climate Change, 8(7), 609-613.](https://www.nature.com/articles/s41586-022-05018-z)

[Pohl, A., Ridgwell, A., Stockey, R. G., Thomazo, C., Keane, A., Vennin, E., & Scotese, C. R. (2022). Continental configuration controls ocean oxygenation during the Phanerozoic. Nature, 608(7923), 523-527.](https://www.nature.com/articles/s41586-022-05018-z)

## Downloading and installing PlaSim-GENIE on EULER
If you are not familiar with with navigating in the terminal, have a look at the instruction manual in the Google Docs. 
The version of PlaSim-GENIE in this repository is taylored to the needs of EULER and will likely not work on other clusters. Have a look at the page 22 (Fig. 1.1) of the [cGENIE reference](https://www.seao2.info/cgenie/docs/muffin.pdf) manual to get an overview over the structure of the directories that cGENIE uses.

To get started, go to the desired directory that you want to install cGENIE in, e.g. `cd /cluster/work/gfd/username`

In order to download the model from the repository you can type:

`git clone git@gitlab.ethz.ch:nwerner/bte_climate_modelling.git`

However this will give you the full repository (which is fine for now but might contain other things in the future). Another option is to download the plasim-genie directory as a `.zip` or `.tar` archive (go for `.tar`).
If you want to do it that way, click on the `plasim-genie` directory click on **Code** and select the type of archive you want to download the directory in.

![Downloading plasim-genie directory as compressed archive](images/download_directory.png)

#### Get paleogeographical reconstructions

So far this models setup has only been tested for the PANALESIS paleogeographic reconstruction by Christian Vérard. There are only some script that need slight adaptation to be able to run it also for other paleogeographies such as Scotese. Get in touch with me if you want to run simulations for other paleogeographies than PANALESIS and I can adapt the scripts (which I will do soon or later anyways). Due to the size of the files for the whole Phanerozoic, the PANALESIS file are only available on request (niklas.werner@erdw.ethz.ch). To make it work with PlaSim-GENIE, you have to copy the PANx folder in which the NetCDF files containing the paleogeography live into into the directory `/cluster/work/gfd/username/cgenie/paleogeography/panalesis/` (`mkdir panalesis` if you don't have the directory, although it should be there). Make sure the `nc` files are finally in the directory `/cluster/work/gfd/username/cgenie/paleogeography/panalesis/PANx`.

To copy them from your local machine to the cluster you can use:

`scp *.nc username@euler.ethz.ch:/cluster/work/gfd/username/cgenie/paleogeography/panalesis/PANx/.`

Note that for this approach you have to be in the same directory as the nc files on your local machine and you must have created the PANx directory beforehand. Other option are copying a zip archive or using an ftp client. 

#### Set proper paths
In order to make the model work for you, you have to change some paths names in some files in the interior of the model. Follow these steps:
+ Navigate to genie-main directory by `cd genie-main` (given you are in `/cluster/work/gfd/username/cgenie` right now).
+ Use your favorite terminal based text editor to open open the `user.sh` file. I use `vim` which woul give you `vim user.sh`. Operating vim is not completely straight forward and you may want to consult some online tutorial (e.g. [here](https://devhints.io/vim)). What you need to know for now is:
- press `i` to get into insert mode; then you can navigate normally using the arrow keys and delete and insert things (`command + c` and `command + v` might not work depending on your terminal)
- in `user.sh` you have four lines where directory paths are specified. These need to be changed to your directories, e.g. `CODEDIR=/cluster/work/gfd/username/cgenie/cgenie.muffin`. Probably you'll only need to replace my username with yours (and might remove the additional `models` directory).
- to save the changes and leave vim press `esc`, then just type `:wq`. That should to it.
- now `vim user.mak`
- again, change the paths for `GENIE_ROOT` (which should be your cgenie.muffin directory) and `OUT_DIR` (cgenie_output)

That's basically it, now we can have a test run.

#### Make a test run
The model is run using xml configuration files that live in the `genie-main/configs` directory. See see Google Docs documentation for an examplanation of the xml files (not yet there). If you are in the genie-main directory you can type (or better copy)

`./bakemuffin_euler_local.sh muffintest.xml`

You should then see something happening. If you get an error, check whether you put in all the paths correctly. Output files are stored in the cgenie_output directory and a new directory with the name of the run is created there.
You can interupt the interactive run with `control + c`.

After the test run (doesn't matter whether it finished or has been interupted) type the following:

`make cleanall`.

### How to set up an experiment
