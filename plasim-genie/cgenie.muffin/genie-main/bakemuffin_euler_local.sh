#!/bin/bash -li

lmod2env
module load modules  
module load intel/16.0.0  
module load open_mpi/1.6.5  
module load netcdf/4.3.1
module load gcc/4.8.2 python/2.7.9 
export NETCDF_DIR=/cluster/apps/netcdf/4.3.1/x86_64/gcc_4.8.2/openmpi_1.6.5
export LD_LIBRARY_PATH=${NETCDF_DIR}/lib:${LD_LIBRARY_PATH}
export CPPFLAGS=-I${NETCDF_DIR}/include
export LDFLAGS=-L${NETCDF_DIR}/lib

./genie.job -f configs/$1
