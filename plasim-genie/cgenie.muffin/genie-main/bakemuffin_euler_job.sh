#!/bin/bash -li

# script to run cGENIE with PlaSim on EULER cluster as a job
# author: Niklas Werner

##########################
####### User Input #######
##########################

timesteps=( 250 ) # default: all timesteps; can be a list eg. (250 200 150); make sure the timesteps are actually available in the input directory
topo=(  ) # default: panalesis
t_total=3000 # total time in years; no default value

# add cgenie modules
biogem=false # default: false
rokgem=false # default: false
sedgem=false # default: false
atchem=false # default: false

# specify whether you want to use a already spun-up model
spinup=false # default: false

# some common parameters to change
# --------------------------------
# NOTE: There is not much sense in running spin-up simulations with different pCO2 values (or any other values that are not major changes in the boundary conditions)
# ---> Run spin-up for different topography, timesteps, or modules and THEN run short simulations (e.g. 200 yrs) with different pCO2 values

pco2=(  ) # default: 280; can be a list eg. (280 560 840)

###############################################################################
####### Don't edit below this line (unless you know what your're doing) #######
###############################################################################

# check whether topo is empty, if so use default value
if [[ -z $topo ]]; then
    echo "Using default topography PANALESIS"
    topo=panalesis
fi
# check whether timesteps is empty, if so run all time steps
if [[ -z $timesteps ]]; then
    echo "Running cGENIE for all time steps! Are you sure that's what you want?"
    # make user type "y" to continue
    read -p "Continue? (y/n) " -n 1 -r
    # if topo = panalesis, get substring from each file and use for 'timestep' array
    if [[ $topo == "panalesis" ]]; then
            for file in ../../paleogeography/$topo/PANx/*.nc; do
                # get substring from last 3 characters before file extension
                timesteps+=(${file: -7:4})
            done
            echo "Running cGENIE for the following time steps: ${timesteps[@]}"
        fi
else
    echo "Running cGENIE for the following time steps: ${timesteps[@]}"
fi
# check whether pco2 is empty, if so use default value
if [[ -z $pco2 ]]; then
    echo "Using default pCO2 value of 280 ppm"
    pco2=280
else
    echo "Running cGENIE for the following pCO2 values: ${pco2[@]}"
fi

# --------------------------
# Loop through all timesteps
# --------------------------

for t in ${timesteps[@]}; do
    # Loop through all pCO2 values
    for co2_conc in ${pco2[@]}; do
        # check whether timesteps are available in the input directory
        if [[ ! -f ../../paleogeography/$topo/PANx/PANx${t}.nc ]]; then
            echo "PANx${t}.nc does not exist in ../../paleogeography/$topo/PANx/"
            exit 1
        else
        # create simulation name: first five letters of topo +  three number for timestep (8 digits in total)
        world_name=${topo:0:5}${t}
        echo "World name: $world_name"

        sim_prefix=pl_go_gs
        # create xml configuration file name (if biogem=true, add _bg to the name, if rokgem=true, add _rg to the name, if sedgem=true, add _sg to the name, if atchem=true, add _ac to the name)
        if [[ $biogem == true ]]; then
            sim_prefix=${sim_prefix}_bg
        fi
        if [[ $rokgem == true ]]; then
            sim_prefix=${sim_prefix}_rg
        fi
        if [[ $sedgem == true ]]; then
            sim_prefix=${sim_prefix}_sg
        fi
        if [[ $atchem == true ]]; then
            sim_prefix=${sim_prefix}_ac
        fi

        # create xml file name

        expid=${sim_prefix}_${world_name}_${t_total}yrs_${pco2}ppm
	xml_file=${expid}.xml
        echo "XML file name: $xml_file"
	
	
        #----------------------------------------------
        # calculate solar luminiosty for each time step
        #----------------------------------------------

        SOLadj=`awk "BEGIN { t_start=4700; t=${t}; L_now=1368; SOLadj=((1 + ((2 / 5) * (1 - ((t_start - t) / t_start ) ) ) )^( -1 ) ) * L_now ; print SOLadj}"`
        echo "Solar luminosity for $t Ma: $SOLadj"

        # --------------------
        # insert into xml file
        # --------------------
        # EXPID; experiment identifier
        
        sed -i "s/\(name=\"EXPID\">\)[^<]*/\1$expid/" plasim-genie_config_template.xml

        # koverall; total time in atmosphere timesteps (for plasim = 45 min)
        koverall=`awk "BEGIN { print  $t_total * 32 * 360 }"`
        sed -i "s/\(name=\"koverall\">\)[^<]*/\1$koverall/" plasim-genie_config_template.xml

        # indir_name; name of plasim input directory
        sed -E -i "s|(<param name=\"indir_name\"><varref>RUNTIME_ROOT<\/varref>\/genie-plasim\/data\/input\/).*|\1$expid</param>|" plasim-genie_config_template.xml       

        # 'world' for goldstein and goldsteinseaice
        sed -i "s/\(name=\"world\">\)[^<]*/\1$world_name/" plasim-genie_config_template.xml

        # solar luminosity
        sed -i "s/\(name=\"gsol0\">\)[^<]*/\1$SOLadj/" plasim-genie_config_template.xml

        # pCO2
        sed -i "s/\(name=\"co2\">\)[^<]*/\1$co2_conc/" plasim-genie_config_template.xml

        # whatever else you want to change...

        # copy xml configuration file to genie-main/configs
        cp plasim-genie_config_template.xml configs/$xml_file

        # ----------------------------------
        # check if boundary conditions exist
        # ----------------------------------
        # check whether all needed boundary conditions already exist, else run create_plasim-genie_BC.sh
        # check whether $indir_name.k1, $indir_name.psiles, $indir_name.paths exist in genie-goldstein/data/input
        go_dir=../genie-goldstein/data/input
        gs_dir=../genie-goldsteinseaice/data/input
        pl_dir=../genie-plasim/data/input/$expid

	echo 	
	echo '##### Checking for input files #####'

        if [[ ! -f $go_dir/$world_name.k1 ]]; then
            echo "$world_name.k1 does not exist in GOLDSTEIN."
        fi

        if [[ ! -f $go_dir/$world_name.psiles ]]; then
            echo "$world_name.psiles does not exist in GOLDSTEIN."
        fi

        if [[ ! -f $go_dir/$world_name.paths ]]; then
            echo "$world_name.paths does not exist in GOLDSTEIN."
        fi

        if [[ ! -f $gs_dir/$world_name.k1 ]]; then
            echo "$world_name.k1 does not exist in GOLDSTEINSEAICE."
        fi

        if [[ ! -f $pl_dir/N032_surf_0129.sra ]]; then
            echo "N032_surf_0129.sra does not exist."
        fi

        if [[ ! -f $pl_dir/N032_surf_0172.sra ]]; then
            echo "N032_surf_0172.sra does not exist."
        fi

        if [[ ! -f $pl_dir/N032_surf_0232.sra ]]; then
            echo "N032_surf_0232.sra does not exist."
        fi

        # Check if any file is missing and run create_plasim-genie_BC.sh if needed
        if [[ ! -f $go_dir/$world_name.k1 || ! -f $go_dir/$world_name.psiles || ! -f $go_dir/$world_name.paths ||
              ! -f $gs_dir/$world_name.k1 || ! -f $pl_dir/N032_surf_0129.sra || ! -f $pl_dir/N032_surf_0172.sra || ! -f $pl_dir/N032_surf_0232.sra ]]; then
		
	    echo $topo $expid $world_name $t
            read -p "Create boundary conditions? (y/n) " -n 1 -r
            cd bc_scripts/
            ./create_plasim-genie_BC.sh $topo $expid $world_name $t
            cd ../
        fi

        # --------------------
        # run cGENIE
        # --------------------
        # load necessary modules from EULER
        lmod2env
        module load modules
        module load intel/16.0.0
        module load open_mpi/1.6.5
        module load netcdf/4.3.1
        module load gcc/4.8.2 python/2.7.9
        export NETCDF_DIR=/cluster/apps/netcdf/4.3.1/x86_64/gcc_4.8.2/openmpi_1.6.5
        export LD_LIBRARY_PATH=${NETCDF_DIR}/lib:${LD_LIBRARY_PATH}
        export CPPFLAGS=-I${NETCDF_DIR}/include
        export LDFLAGS=-L${NETCDF_DIR}/lib

        #sbatch -n 16  --mem-per-cpu=1024 --time=48:00:00 ./genie.job -f configs/$xml_file
	sbatch -n 16 --time=48:00:00 --mem-per-cpu=1024 --job-name=$expid ./genie.job -f configs/$xml_file
         fi
    done
done

