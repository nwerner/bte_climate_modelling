#!/bin/bash

# MATLAB command to call the script with the provided filename
matlab -nosplash -nodisplay -r "convertNetCDFtoMat('$1','$2'); exit;"



