function convertNetCDFtoMat(input_filename,var)
    % Get the file parts (path, name, and extension)
    [filepath, filename, fileext] = fileparts(input_filename);
   
    disp(filepath)
    disp(filename)
    disp(fileext) 
    
    cd('../../../muffingen/input/');
    
    % Get the current working directory
    %currentDir = pwd;

    % Combine the current directory and relative path using fullfile
    %fullPath = fullfile(pwd, outdir);

    % Check if the directory exists, and if not, create it
    %if ~exist(fullPath, 'dir')
    %   print('No such directory:', fullPath);
    %end
    
    disp(input_filename)          
    disp(var)
    % Construct the output file name with the same path and name but different extension
    output_filename = [filename '.mat']

    % Read data from the NetCDF file
    data = ncread(input_filename, var); % Replace 'variable_name' with the actual variable name in your NetCDF file
    
    % Save the data to a MATLAB .mat file with the new name and extension
    data_dims_swapped = permute(data,[2 1]);
    save(output_filename,'data_dims_swapped');

    fprintf('Conversion complete. Saved as %s\n', output_filename);
end


