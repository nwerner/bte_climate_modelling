# Convert netcdf topography files (e.g. PANALESIS) to PlaSim inputs
# Scripts is based on work by Julian Rogger and here only converted in python by Niklas Werner
#########################
import numpy as np
import xarray as xr
import os
import glob
import sys
from datetime import datetime
import matplotlib.pyplot as plt
import pandas as pd
#########################

res_user = 'T21'  # default grid resolution for PlaSim GENIE coupling
outdir = str(sys.argv[1]) # specify output directory
topo_var = str(sys.argv[2]) # specify topography variable name

print('Output directory: ' + outdir)
print('Topography variable: ' + topo_var)

# define functions
#-----------------
def insert_header(file_path, line, position):
        # Read the contents of the file into a list
        with open(file_path, 'r') as file:
            lines = file.readlines()

        # Insert the new line at the specified position
        lines.insert(position, line + '\n')

        # Write the modified list back to the file
        with open(file_path, 'w') as file:
            file.writelines(lines)

if res_user == 'T21':
    lat_target = 32
    lon_target = 64
else:
    sys.exit('ERROR: resolution not supported')

# change 'path' to the directory where the topography files are located
print(os.getcwd())
os.chdir(outdir)
print(os.getcwd())

file_list = glob.glob(topo_var + '_64x32_tmp.nc')

for file in file_list:
    print(file)
    # read in topography file
    data = xr.open_dataset(file)
    topo = data[topo_var].values

    # NOTE: PlaSim coupled to GENIE need at least the following surface data inputs:
    # - land sea mask [fraction]; code 172
    # - orography [m]; code 129
    # - glacier fraction (?maybe?); 232

    # land sea mask
    # -------------
    # 0 = ocean, 1 = land
    lsm = np.zeros_like(topo)
    lsm[topo <= 0] = 0 # ocean
    lsm[topo > 0] = 1 # land

    # orography
    # ---------
    orography = np.zeros_like(topo)
    orography[topo <= 0] = 0 # ocean
    orography[topo > 0] = topo[topo > 0] # land
 
    # glacier fraction
    # so far just initialized without without landice

    # combine into one dataset
    ds_combined = xr.Dataset(
        {
            "0172_lsm": (("lat", "lon"), lsm),
            "0129_orography": (("lat", "lon"), orography),
        },
        coords={
            "lat": data['lat'].values,
            "lon": data['lon'].values,
        },
    )

    # convert to ascii service format (.sra)
    for var in ds_combined.variables:
        if var not in ['lat', 'lon']:
            # ds_combined[var].plot()
            #plt.show()
   
            var_array = ds_combined[var].values
            print(var_array)
            print(outdir)
            var_array = np.flipud(var_array) # flip array to match PlaSim convention (reversed latitudes)
            filename = os.path.join(outdir, f"N032_surf_{var[0:4]}.sra")
            print(filename)  
            # Write the data to the ASCII file
            np.savetxt(filename, var_array, fmt="%.3e", delimiter="\t")
            print(f"Saved {filename}")

            # Example usage
            header = f'{var[1:4]} \t 0 \t {datetime.now().strftime("%Y%m%d")} \t {0} \t {lon_target} \t {lat_target} \t 0 \t 0'
            insert_header(filename, header, 0)

# test_data_plot = pd.read_csv(filename, sep='\t', header=None, skiprows=1)
# plt.pcolor(test_data_plot)
# plt.show()

