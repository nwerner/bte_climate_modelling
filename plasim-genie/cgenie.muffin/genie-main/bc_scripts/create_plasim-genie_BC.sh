#!/bin/bash -li

# This script so far runs only for panalesis!
lmod2env
module load matlab
module load cdo
 
# This script creates boundary conditions for PlaSim-GENIE
# files needed by plasim are created by topo2bc.py and for cGENIE using muffingen

############### User Input #####################
# specify paleogeographic reconstruction; must match a diretory name in /Users/nwerner/Data/base-top-earth/plasim-genie_BC
topo=$1


# specify timestep (e.g. 250, e.i 250 Ma); if empty all time steps will be processed
sim_name=$2
# specify simulation name
world_name=$3
# specify timestep
timestep=$4
#################################################

# input directory
# if topo is panalesis the indir needs an additional PANx after $topo
if [[ $topo == "panalesis" ]]; then
    indir=../../../paleogeography/$topo/PANx/
else
  echo 'So far only PANALESIS input is supported. Please specify a the correct directory!'  
fi

# check whether indir exists
if [[ ! -d $indir ]]; then
    echo "Input directory $indir does not exist!"
    exit 1
fi

# Loop through all files that match (either one simulation or all)
# if sim="", get substring from each file in indir and create indiviual output directory for each siom; different location in the filename for different topo
file=$(ls ${indir}*${timestep}.nc)

if [[ $topo == "panalesis" ]]; then
    echo $file

    # Specify output directories
    outdir_goldstein='../../../genie-goldstein/data/input/'
    outdir_plasim=../../genie-plasim/data/input/
    outdir_goldsteinseaice='../../../genie-goldsteinseaice/data/input/'
    dir_muffingen='../../muffingen/'
    
    echo ----------------------------------------------------
    echo          Start remapping for PlaSim... 
    echo ----------------------------------------------------
    # Remap topo netcdf input files to 0, 360, -90, 90 (64x32)
    # -------------------------------------------------------
    # Check if output directory exists, if not create it
    if [[ ! -d ${outdir_plasim}${sim_name} ]]; then
    echo Create directory: ${outdir_plasim}${sim_name}
    mkdir ${outdir_plasim}${sim_name}
    fi
        
    # specify plasim input direcotry & topographic variable name
    # sim=${file: -7:4} # get name of the simulation from filename (e.g. x250)
    indir_plasim=${outdir_plasim}${sim_name}
    topo_var=${file: -7:4}

    echo PlaSim input files directory: ${indir_plasim}

    cdo -L  -remapbil,r64x32 -sellonlatbox,-180,180,-90,90 $file ${indir_plasim}/${sim_name}_64x32_tmp.nc
	    
    echo Successfully remapped $file to: 64 x 32
    echo Saved as ${indir_plasim}/${sim_name}_64x32_tmp.nc

    echo ----------------------------------------------------
    echo     Create PlaSim input files using topo2bc.py
    echo ----------------------------------------------------
    # create plasim files using python script
    # ---------------------------------------
    env2lmod
    module load gcc/6.3.0 python/3.8.5	
    source "myvenv/bin/activate"
    echo PlaSim input files direcotry: ${indir_plasim}
    echo Topographic variable name:  ${topo_var}
 
    python3 topo2bc.py ${indir_plasim} ${topo_var} ${sim_name}
    cp ${outdir_plasim}apm.dat ${indir_plasim}
    cp ${outdir_plasim}N032_surf_0232.sra ${indir_plasim}
    rm ${indir_plasim}/${sim_name}_64x32_tmp.nc

    echo -----------------------------------------------------
    echo          Start remapping for muffingen 
    echo -----------------------------------------------------
    # Remap files for muffingen to 0, 360, -90, 90 (360x180)
    # --------------------------------------------------------
    lmod2env
    module load matlab
    module load cdo
    
    cdo -L  -remapbil,r360x180 -sellonlatbox,-180,180,-90,90 $file ${dir_muffingen}input/${sim_name}_360x180.nc
            
    echo Successfully remapped $file to: 360 x 180
    echo Saved as ${dir_muffingen}input/${sim_name}_360x180.nc

    echo ------------------------------------------------------
    echo              Convert NetCDF to .mat
    echo ------------------------------------------------------
    # Convert netcdf files to .mat since muffingen needs mat
    # ---------------------------------------------------------
    echo $topo_var
    cd nc2mat 
    ./convertNetCDFtoMat.sh ${dir_muffingen}input/${sim_name}_360x180.nc ${topo_var}
    cd ../../../muffingen/
	    
    echo -----------------------------------------------------
    echo             Update muffingen spec file 
    echo -----------------------------------------------------
    # put proper experiment names into BC_template file
    # --------------------------------------------------------

    # input experiment name; 8 characters
    par_wor_name="19s/.*/par_wor_name='$world_name';/"

    # input data
    par_expid="21s/.*/par_expid='${sim_name}_360x180';/"
	    
    echo par_wor_name: $par_wor_name
    echo par_expid: $par_expid

    # replace lines in BC_template.m
    sed -in $par_wor_name BC_template.m
    sed -in $par_expid BC_template.m
            
    echo ----------------------------------------------------
    echo                 run muffingen
    echo ----------------------------------------------------
    # run muffingen
    # -------------

    matlab -nodisplay -nosplash -r "muffingen('BC_template'); exit"
	 
    # remote unnecesarry files from output directory
    cd output/${world_name}/
    cp *.{k1,psiles,paths} ${outdir_goldstein}
    cp *.k1 ${outdir_goldsteinseaice}
    rm *.ps
    rm *.dat
	    
    echo ----------------------------------------------------
    echo        All done! Muffins are ready... Enjoy!
    echo ----------------------------------------------------
		
    elif [[ $topo == "scotese" ]]; then
            sim=${file: -8:3}
            outdir=/Users/nwerner/Data/base-top-earth/plasim-genie_BC/$topo/$sim

            if [[ ! -d $outdir ]]; then
                echo "Creating directory $outdir"
                mkdir -p $outdir
            fi

            # create plasim files
            topo_var=z
            python topo2bc.py $indir $outdir $topo_var $sim

            # use muffingen to create cGENIE files
            # convert topography to mat file first
            cdo remapbil,r360x180 $file /Users/nwerner/muffingen/output/${topo}_${sim}_360x180.nc
            cd /Users/nwerner/muffingen/output/
            nc2mat ${topo}_${sim}_360x180.nc${topo}_${sim}_360x180.mat
            cd ..

            # input experiment name; 8 characters
            par_wor_name="19s/.*/par_wor_name='pnls_${$sim}';/"
            # input data
            par_expid="21s/.*/${topo}_${sim}_360x180.mat';/"
            # replace lines in BC_template.m
            gsed sed -in $par_wor_name BC_template.m
            gsed sed -in $par_expid BC_template.m

            # run muffingen
            matlab -nodisplay -nosplash -nodesktop -r "muffingen("BC_template"); exit"
        fi
 
    exit 1



