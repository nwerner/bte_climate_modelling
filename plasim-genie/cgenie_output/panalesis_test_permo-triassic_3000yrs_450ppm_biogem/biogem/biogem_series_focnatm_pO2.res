 % time (yr) / global pO2 flux (mol yr-1) / global pO2 density (mol m-2 yr-1)  NOTE: is the atmospheric forcing flux *net* of the sea-air gas exchange flux.
