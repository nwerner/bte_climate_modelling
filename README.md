# Goal
This repository contains things related to the climate modelling section of the SINERGIA project *Base-Top-Earth*. The goal of this project is to work towards a integrated coupling of climate models to the Earth's interior.

# Structure
So far this directory only contains the configuration of the intermediate complexity model PlaSim-GENIE that runs on the ETH EULER cluster. Go into its directory to find information on how to install and run it.